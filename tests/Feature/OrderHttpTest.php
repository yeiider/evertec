<?php

namespace Tests\Feature;

use App\Models\Order;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class OrderHttpTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Crear una orden
     *
     * @test
     */

    public function test_create_order_http()
    {
        $response = $this->post(route('store.order'),[
            '_token' => csrf_token(),
            'customer_name' => "Test Name",
            'customer_email' => "test@test.com",
            'customer_mobile' => "111 111 1111",
        ]);
        $response->assertStatus(302);
    }

    /**
     * Generar orden de pago en placetopay
     * 
     * @test
     */

    public function test_start_session_service_placetopay()
    {
        $order = factory(Order::class)->create(['code'=>'Agf34HSqsE']);
        $response = $this->post(route('pay',$order->code),[
            '_token' => csrf_token(),
        ]);
        $response->assertStatus(302);
    }

    /**
     * Lista todas las ordenes
     * 
     * @test
     */

    public function test_list_all_order()
    {
        $response = $this->get(route('list'));
        $response->assertStatus(200)
        ->assertSee('Codigo de la orden');
    }

    /**
     * Respuesta entregada por el servicio de placetopay
     * 
     * @test
     */

    public function test_response_service_api()
    {
       $response = $this->getJson(route('response.api'),[
           'status' => ["status"=>"APROVED"],
           'requestId'=>123123
       ]);
       $response->assertStatus(302);
    }
}
