<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Order;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'code' => Str::random(10),
        'reference' => rand(10000000,99999999),
        'order_price' => rand(10000000,99999999),
        'customer_name' => $faker->name,
        'customer_email' => $faker->safeEmail,
        'customer_mobile' => $faker->phoneNumber,
    ];
});
