<?php

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeedr extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name' => "IdeaPad L340 Gaming 15.6, Intel",
            'price' => 3099000,
            'description' => "<h3>Un equipo listo para jugar. ¿Te apuntas?</h3><p>Cuando hablamos de videojuegos, lo importante es seleccionar la opción correcta. Con la IdeaPad L340 Gaming de 15.6”, estarás tomando la mejor decisión desde el principio. Equipada con hasta los últimos procesadores Intel® Core™ i7, tarjetas gráficas NVIDIA® GeForce® de hasta las últimas generaciones y un espectacular sonido Dolby Audio™ (estas características varían según el modelo, revisa la configuración de tu equipo antes de la compra), disfrutarás de toda la potencia y el rendimiento que necesitas. Además, estarás preparado para cualquier rival, cualquier partida y cualquier escenario. La unidad óptica y la retroiluminación LED del teclado son opcionales; la pantalla puede variar según el equipo.</p>",
            'file' => "product.webp" 
        ]);
    }
}
