# Instalacion de prueba Evertec

Primero se debes tener instalado compouser 

``apt-get install composer``


Luego debes clonar el proyecto 


``git clone https://bitbucket.org/yeiider/evertec.git``


 Entra a la terminal en la raiz del proyecto 
 
 ``composer install``

Has una copia del archivo .env.example guardalo como .env 

``sudo cp .env.example .env``


Luego ejecuta el siguiente comando 

``php artisan key:generate``


 En el archivo .env que copiastes ingresa los siguientes valores 

``` 
API_LOGIN_PLACETOPAY=USER_LOGIN_PLACETOPAY
API_TRANKEY_PLACETOPAY=TRAN_KEY_PLACETOPAY
API_URL_PLACENTOPAY=URL__SERVICE
```
### Base de datos
En el mismo archivo pones tus credenciales de la base de datos

```
DB_DATABASE=nombre_base_de_datos
DB_USERNAME=usuario
DB_PASSWORD=contraseña
```

 Luego ejecuta los siguientes comandos

> ``php artisan migrate``
> ``php artisan db:seed``

 Debes hacer uso de las CRON configura una corn para correr los JOB

 ### Aplicacion lista para usar
