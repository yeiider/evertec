@extends('evertec.layouts.layout')

@section('main')
@if (session('message'))
    {!! session('message') !!}
@endif

<div class="container">
    <div class="row mt-5">
        <div class="col-xs-12 col-sm-6 col-lg-6">
            <div class="item row">
                <div class="col-auto">
                    <strong>Referencia: </strong>
                </div>
                <div class="col-auto">
                    {{ $order->reference }}
                </div>
            </div>
            <div class="item row">
                <div class="col-auto">
                    <strong>Nombre Cliente: </strong>
                </div>
                <div class="col-auto">
                    {{ $order->customer_name }}
                </div>
            </div>
            <div class="item row">
                <div class="col-auto">
                    <strong>Email Cliente: </strong>
                </div>
                <div class="col-auto">
                    {{ $order->customer_email }}
                </div>
            </div>
            <div class="item row">
                <div class="col-auto">
                    <strong>Fecha: </strong>
                </div>
                <div class="col-auto">
                    {{ $order->created_at }}
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-lg-6">
            <div class="item row">
                <div class="col-auto">
                    <strong>Empresa: </strong>
                </div>
                <div class="col-auto">
                    Evertect
                </div>
            </div>
            <div class="item row">
                <div class="col-auto">
                    <strong>Email: </strong>
                </div>
                <div class="col-auto">
                    email@evertect.com
                </div>
            </div>
            <div class="item row">
                <div class="col-auto">
                    <strong>Telefono: </strong>
                </div>
                <div class="col-auto">
                    111 1111 11111
                </div>
            </div>
            <div class="item row">
                <div class="col-auto">
                    <strong>Estado de orden: </strong>
                </div>
               @switch($order->status)
                   @case('CREATED')
                   <div class="col-auto rounded-pill bg-info text-white">
                    <strong>CREADA</strong>
                    </div>
                   @break
                   @case('APPROVED')
                   <div class="col-auto rounded-pill bg-success text-white">
                    <strong>PAGADA</strong>
                    </div>
                       @break
                   @case('PENDING')
                   <div class="col-auto rounded-pill bg-warning text-white">
                    <strong>PENDIENTE</strong>
                    </div>
                       @break
                    @case('REJECTED')
                    <div class="col-auto rounded-pill bg-danger text-white">
                        <strong>RECHAZADO</strong>
                        </div>
                       @break
                   @default
               @endswitch
            </div>

        </div>
    </div>

    <div class="table mt-3">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Foto</th>
                    <th>Cantidad</th>
                    <th>Valor</th>
                    <th>Total Pagar</th>
                </tr>
            </thead>
            <tbody>
                @foreach($order->orderProducts as $order_product)
                    <tr>
                        <td>
                            <div class="col-4">
                                <img src="{{ asset('img/'.$order_product->product->file ) }}"
                                    class="img-fluid" alt="product logo">
                            </div>
                            <strong>{{ $order_product->product->name }}</strong>
                        </td>
                        <td>{{ $order_product->qty }}</td>
                        <td>${{ number_format($order_product->product->price,2,',','.') }}</td>
                        <td>${{ number_format($order_product->product->price,2,',','.') }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <h5>Metodos de pago</h5>
        <hr>
        <div class="col-sm-4 mt-3 mt-3 col-md-2 col-xs-5">
            @if ($order->status!="APPROVED")
            <form action="{{ route('pay',$order->id) }}" id="pay" method="POST">
                @csrf
                </form>
                <a href="#" onclick="pay.submit()"><img src="https://static.placetopay.com/placetopay-logo.svg" class="img-fluid" ></a>
            @endif
          
        </div>
    </div>
</div>
@endsection
