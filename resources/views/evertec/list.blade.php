@extends('evertec.layouts.layout')

@section('main')
<div class="container mt-5">
    <table class="table table-hober">
        <thead>
           <tr>
              <th>Codigo de la orden</th>
              <th>Cliente</th>
              <th>Email</th>
              <th>Telefono</th> 
              <th>Estado</th>
              <th>Opciones</th>
           </tr>    
        </thead> 
        <tbody>
        @foreach ($orders as $order)
            <tr>
                <td>{{ $order->code }}</td>
                <td>{{ $order->customer_name }}</td>
                <td>{{ $order->customer_email }}</td>
                <td>{{ $order->customer_mobile }}</td>
                <td>{{ $order->status }}</td>
                <td>
                    <a href="{{ route('show.order',$order->code) }}" class="btn btn-default">
                        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                            <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                        </svg>
                    </a>
                </td>
            </tr>
        @endforeach    
        </tbody>   
    </table>    
</div>    
@endsection