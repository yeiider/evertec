@extends('evertec.layouts.layout')

@section('main')
    <div class="title mt-5">
        <h2>{{ $product->name }}</h2>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6 col-lg-6 col-xs-12 border p-3">
            <img src="{{ asset('img/'.$product->file) }}" class="img-fluid" alt="">
            <details>
                <summary>Ver detalles del equipo!</summary>
                {!! $product->description !!}
            </details>
        </div>
        <div class="col-md-6 col-lg-6 col-xs-12">
            <div class="price text-right border-right p-2">
                <h3>${{ number_format($product->price,2,',','.') }} <small class="text-success">20% off </small></h3>
            </div>
            <form action="{{ route('store.order') }}" method="POST">
                @csrf
                <h2>Complete la información</h2>
                <div class="form-group">
                    <label for="name">Nombre</label>
                    <input type="text" class="form-control" id="name" name="customer_name" autocomplete="off" required>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" name="customer_email" autocomplete="off" required>
                </div>
                <div class="form-group">
                    <label for="telefono">Telefono</label>
                    <input type="tel" class="form-control" id="telefono" name="customer_mobile" autocomplete="off" required>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary col-12"  >COMPRAR</button>
                </div>
            </form>
        </div>
    </div>
@endsection