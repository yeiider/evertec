<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ProductsController@index')->name('index');
Route::post('/crear/orden','OrdersController@store')->name('store.order');
Route::get('/orden/{code}','OrdersController@showOrder')->name('show.order');
Route::post('/pagar/orden/{id}','OrdersController@pay')->name('pay');
Route::get('/ordenes','OrdersController@index')->name('list');
Route::get('/respuesta/api/placetopay/{reference}','OrdersController@responseApiPlacetopay')->name('response.api');
