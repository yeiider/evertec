<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use app\Http\Controllers\ApiService;
use App\Models\Order;

class PendingPayment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $order;
    /**
     * Create a new job instance.
     * 
     * @return void
     */
    public function __construct($order)
    {
        $this->order=$order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $apiService= new ApiService();
        $res = $apiService->getRequestInformation($this->order->session_status);
        $this->order->status=$res['status']['status'];
        $this->order->save();
        
    }
}
