<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Support\Str;

class ApiService extends Controller{

    private  $baseUrl;
    private  $login;
    private  $secretKey;
    private  $seed;
    private  $tranKey;
    private  $auth;

    function __construct()
    {
       $this->login = config('services.placetopay.api_login_placetopay');
       $this->baseUrl = config('services.placetopay.api_url_placetopay');
       $this->secretKey = config('services.placetopay.api_trankey_placetopay');
       $this->seed= date('c');
       $nonce = md5( Str::random(15));
       $this->tranKey = base64_encode(sha1($nonce . $this->seed . $this->secretKey,true));
       $this->auth=[
         'login' => $this->login,
         'tranKey' => $this->tranKey,
         'nonce' => base64_encode($nonce),
         'seed'  => $this->seed,
       ];
    }
  /**
   * Send Request to placetopay
   * 
   * @param Order
   * @return array
   * 
   */
    public function sendRequest(Order $order)
    {
       $reference = $order->reference;
       $request=[
        'auth' => $this->auth,
        'locale' => 'es_CO',
        "buyer" => [
            "name"=> $order->customer_name,
            "email"=> $order->customer_email,
            "mobile"=> $order->customer_mobile
        ],
          'payment' => [
              'reference' => $reference,
              'description' => 'IdeaPad L340 Gaming ',
              'amount' => [
                'currency' => 'COP',
                'total' => $order->order_price
              ],
              "allowPartial"=>false,
            ],
            'expiration' => date('c', strtotime('+2 days')),
            'returnUrl' =>  route('response.api',$reference),
            'ipAddress' => '127.0.0.1',
            'userAgent' => $_SERVER['HTTP_USER_AGENT'],

        ];
      return  $this->requestApi('api/session',$request);
    }

    /**
     * GetRequestInformation
     * @param int $requestId
     * @return array
     */

     public function getRequestInformation($requestId)
     {
      
       return $this->requestApi('api/session/'.$requestId,array('auth'=>$this->auth));
     }
    
     /**
      * RequetsApi
      * @param string $url_service
      * @param string $data
      * @return array
      *
      */
     private function requestApi($urlService,$data)
     {
      $curl = curl_init();
      curl_setopt_array($curl, array(
        CURLOPT_URL => $this->baseUrl.$urlService,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($data),
        CURLOPT_HTTPHEADER => array(
          "Content-Type: application/json"
        ),
      ));
      
      $response = curl_exec($curl);
      
      curl_close($curl);
      $res = json_decode($response,true);
      return $res;
     }
}
