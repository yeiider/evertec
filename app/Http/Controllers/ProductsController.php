<?php

namespace App\Http\Controllers;

use App\Models\Product;

class ProductsController extends Controller
{
    public function index(){
        $product = Product::find(1);
        return view('evertec.index',compact('product'));
    }
}
