<?php

namespace App\Http\Controllers;

use App\Jobs\PendingPayment;
use App\Models\Order;
use App\Models\OrderProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class OrdersController extends Controller
{

    private $serviceApi;
    public function __construct()
    {
        $this->serviceApi=new ApiService();
    }
    /**
     * Index list all order
     * 
     * @param NULL
     * @return void
     */

    public function index()
    {
       $orders = Order::all();
       return view('evertec.list',compact('orders'));
    }


    /**
     * store Order 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     * 
     */

    public function store(Request $request)
    {
        $rules=[
            'customer_name' => 'required|string|max:80',
            'customer_email' => 'required|email|max:120',
            'customer_mobile' => 'required|string|max:40',
        ];
        $this->validate($request,$rules);
        $order=Order::create([
            'code' => Str::random(10),
            'reference' => rand(10000000,99999999),
            'order_price' => 3990000,
            'customer_name' => ucwords($request->customer_name),
            'customer_email' => $request->customer_email,
            'customer_mobile' => $request->customer_mobile,
        ]);
        if($order){
            OrderProduct::create([
                'order_id' => $order->id,
                'product_id' => 1
            ]);
        return redirect(route('show.order',$order->code));
        }
        
    }
  /**
   * Show Order
   * @param string
   * @return void
   */
    public function showOrder($code){
        $order=Order::where('code','=',$code)->first();
        return view('evertec.show',compact('order'));
    }

    /**
     * Generate session of pay in placetopay
     * 
     * @param \Illuminate\Http\Request  $request
     * @param string $id
     * @return void
     */
    public function pay(Request $request,$id)
    {
       $order= Order::find($id);
       if($order->process_url && $order->status=="PENDING")
           return redirect($order->process_url);
       if($order->status=="APPROVED")
          return redirect()->back()->with('message',"<div class='alert alert-info mt-5 alert-dismissible fade show' role='alert'>
          <strong>Pagada!</strong>Esta orden ya fue pagada!.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
        </div>");
       $rest = $this->serviceApi->sendRequest($order);
       if($rest['status']['status']!='OK'){
        $message="<div class='alert alert-danger mt-5 alert-dismissible fade show' role='alert'>
        <strong>Error!</strong>Estamos teniendo errores intente mas tarde.
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
          <span aria-hidden='true'>&times;</span>
        </button>
      </div>";
      return redirect()->back()->with('message',$message);
       }
        
        $order->session_status = $rest['requestId'];
        $order->status="PENDING";
        $order->process_url=$rest['processUrl'];
        $order->save();
        return redirect($rest['processUrl']);
    }

    /**
     * Get Response of Service placetopay 
     * 
     * @param int $reference
     * @return void
     */
    public function responseApiPlacetopay($reference)
    {
        $order=Order::getOrder($reference);
        $response=$this->serviceApi->getRequestInformation($order->session_status);
        $order->status=$response['status']['status'];
        $order->save();
       
        
        switch($response['status']['status'])
        {
            case "APPROVED" :
                $message="<div class='alert alert-success mt-5 alert-dismissible fade show' role='alert'>
                <strong>Pago registrado!</strong>Tu pago ha sido registrado exitosamente!
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                  <span aria-hidden='true'>&times;</span>
                </button>
              </div>";
            break;
            case "REJECTED" :
                $message="<div class='alert mt-5 alert-danger alert-dismissible fade show' role='alert'>
                <strong>Pago rechazado!</strong>Su pago ha sido rechazado.
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                  <span aria-hidden='true'>&times;</span>
                </button>
              </div>";
            break;
            
            case "PENDING" :
                PendingPayment::dispatch($order)->delay(now()->addMinutes(10));
                $message="<div class='alert mt-5 alert-warning alert-dismissible fade show' role='alert'>
                <strong>Esperando resouesta!</strong>Su pago se encuentra en proceso espere la notificacion en 10 minutos!.
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                  <span aria-hidden='true'>&times;</span>
                </button>
              </div>";
            break;
        }
       
        return redirect(route('show.order',$order->code))->with('message',$message);
    }
}
