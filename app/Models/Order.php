<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */ 
    protected $fillable=[
        'code',
        'reference',
        'order_price',
        'session_status',
        'process_url',
        'customer_email',
        'customer_mobile',
        'customer_name'
    ];
    /**
     * get all product of relactions
     * 
     * @return Product
     */
    public function orderProducts()
    {
        return $this->hasMany(OrderProduct::class,'order_id','id');
    }
    static function getOrder($reference)
    {
       return Order::where('reference','=',$reference)->first();
    }
}
