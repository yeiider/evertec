<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */ 
   protected $fillable=[
    'order_id',
    'product_id',
    'qty'
   ];

   public function product()
   {
     return $this->hasOne(Product::class,'id','product_id');
   }
}
